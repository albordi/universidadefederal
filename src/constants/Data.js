export const recipientsProd = [
  // { name: 'Assoc', address: 'asscampogrande@gmail.com'  },
  // { name: 'André Luís Bordignon', address: 'andre.bordignon@ifsp.edu.br' },
  { name: 'André Luís Bordignon', address: 'albordignon@gmail.com' },
];

export const mobTitle =
  'Abaixo Assinado Pela Criação da Universidade Federal de Campinas.';

export const mobSubTitle = 'Campinas merece uma universidade federal';

export const mobDescription =
  'Campinas é uma cidade de mais um milhão de habitantes e até hoje não tem uma universidade federal. Acreditamos que já é hora de se inaugurar uma nova universidade por aqui. Por esse motivo esse abaixo assinado se soma a outras iniciativas na busca dessa realização. ';

export const mobText = {
  paragraph1:
    'A necessidade da cidade de Campinas contar com uma Universidade Federal se justifica pois, a cidade se espalha em seu território e se expande para a região metropolitana, onde milhares de jovens, principalmente vindos da escola pública necessitam de formação universitária para ingressar no mercado de trabalho e o próprio mercado de trabalho da região necessita de força de trabalho qualificada e não são plenamente atendidos pelas instituições presentes na cidade.',
  paragraph2:
    'Campinas possui mais de 1,2 milhões de habitantes, está entre os 10 maiores municípios do Brasil, produz 15% da ciência e tecnologia nacional, mas ao mesmo tempo em regiões como o Campo Grande e o Ouro Verde, que concentra mais de um quarto da população do município, não possui ensino universitário público para a juventude. São jovens trabalhadores, vindos de escola pública que não tem acesso ao ensino universitário.',
  paragraph3:
    'Por esses e outros motivos estamos coletando assinaturas para enviar juntamente com o texto abaixo para o ministro da educação.',
  paragraph4: 'Se você apoia essa causa junte-se a nós e assine.',
  paragraph5:
    '',
  paragraph6:
    '',
  paragraph7: '',
};

export const email = {
  from: 'mobilizaredescobrir@gmail.com',
  subject: 'ABAIXO ASSINADO AO MEC (MINISTÉRIO DA EDUCAÇÃO)PELA CRIAÇÃO DA UNIVERSIDADE FEDERAL EM CAMPINAS!',
  message:
    'Exmo Sr. Camilo Santana, Ministro da Educação do Brasil,\n\n Considerando que não há nenhuma universidade pública federal na Região Metropolitana de Campinas-SP (RMC), sendo as mais próximas localizadas a 100 km, na Região Metropolitana de São Paulo, e a cerca de 200 km, em São Carlos;   Considerando a alta demanda por ensino universitário federal verificada em Campinas, que comporta 1.138.309 habitantes (Censo 2022 - IBGE), e em toda a RMC, que conta com 19 cidades e população de 3,3 milhões de habitantes (Censo 2022 - IBGE); Considerando que Campinas e região têm contribuído significativamente para o desenvolvimento nacional, produzindo cerca de 20% do PIB (Produto Interno Bruto) do Estado de São Paulo, com PIB per capita em 2020 de R$ 53.896,97 (IBGE, 2023), sendo responsável por cerca de 15% de toda a produção científica nacional e, no campo da ciência, tecnologia e inovação, dispondo de infraestrutura pública e privada, que faz do município e da RMC referência nacional e internacional; Considerando que, mesmo com o perfil descrito, Campinas não tem recebido como contrapartida a criação de uma universidade federal no território, o que poderia ocorrer articulando-a de modo orgânico à estrutura acadêmico-científica, tecnológica e de inovação instalada em Campinas e região, nos processos de desenvolvimento do ensino, da pesquisa/inovação e da extensão, colaborando, sobremaneira, para ampliar a contribuição ao Estado e ao País, bem como para atender às demandas de formação superior pública, gratuita, laica e de qualidade socialmente referenciada, que não são hoje atendidas pelas Instituições de Ensino Superior (IES) aqui instaladas;   Considerando que, embora a Universidade Federal de Campinas tenha sido pleiteada por ampla campanha realizada na RMC entre os anos de 2013 e 2014, que resultou em tratativas junto ao MEC e, inclusive, no anúncio de criação pelo então Prefeito de Campinas, Sr. Jonas Donizette, em junho de 2014, ela ainda não foi criada e nem sequer nenhum campus de universidade federal existente já instalada no Estado de São Paulo; Considerando, por fim, o amplo consenso regional em torno da proposta de se trazer para a RMC, especialmente para a região do Campo Grande, em Campinas, uma universidade pública federal, pois muito colaborará para o sustentável desenvolvimento econômico, social, político e cultural regional; Nós, abaixo assinados(as), moradores(as) da RMC, articulados(as) à Frente Parlamentar pela Criação da Universidade Federal de Campinas, vimos solicitar seus préstimos no sentido dar os encaminhamentos necessários para viabilizar a criação de uma universidade pública federal em Campinas, conforme consta no processo nº 23123.006333/2023-56, assentado no MEC (Ministério da Educação).',
};

export const contactEmail = 'cecilio.santos@campinas.sp.leg.br';
