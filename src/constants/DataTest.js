export const recipientsTest = [
  { name: 'Assoc', address: 'asscampogrande@gmail.com', position: 'Sem posição'  },
  { name: 'André Luís Bordignon', address: 'andre.bordignon@ifsp.edu.br', position: 'Sem posição' },
  { name: 'André Luís Bordignon', address: 'albordignon@gmail.com', position: 'Contra' },
];

export const mobTitle = 'Não queremos pátio de veículos apreendidos no Satélite !';

export const mobSubTitle = 'Pátio NÃO!!! Esporte, Lazer e Cultura SIM!!!';

export const mobDescription = 'A prefeitura de Campinas quer enviar o pátio de veículos apreendidos para o bairro do Satélite Íris. O que queremos aqui são áreas e atividades de esporte, de lazer e de cultura, como indicou a pesquisa que deu voz à população local, e não um pátio de veículos apreendidos que causará transtornos e trará ainda mais prejuízos à nossa comunidade e do entorno.';

export const mobText = {
  paragraph1: 'De acordo com a reportagem do jornal Correio Popular "Pátio de veículos deve ser transferido para Satélite Íris", publicada no dia 13 de setembro de 2022, o Pátio Municipal de Recolhimento e Guarda de Veículos deve ser transferido para o bairro Satélite Íris. Essa decisão foi tomada sem a realização de um Estudo de Impacto de Vizinhança (EIV), conforme prevê a Lei Federal nº 10.257/2001 que determina a realização de um estudo aprofundado para analisar os impactos de um empreendimento para a comunidade local e entorno.',
  paragraph2 : 'Além disso, em abril de 2022 o coletivo Mobiliza Satélite – Redescobrir para Transformar - MSRT, por meio do Projeto Redescobrir – Satélite Íris, realizou um Diagnóstico Socioterritorial Participativo para levantar com a população local, as principais potências e necessidades do território. Este Diagnóstico apontou a área de Esporte, Lazer e Cultura como a que mais necessita de atenção no Satélite Íris.',
  paragraph3: 'Sendo assim, a resposta que nós damos, mesmo sem ser perguntada, é que somos contra o pátio aqui na região do Satélite Íris.',
  paragraph4: 'O que queremos aqui são áreas e atividades de esporte, de lazer e de cultura, como indicou a pesquisa que deu voz à população local, e não um pátio de veículos apreendidos que causará transtornos e trará ainda mais prejuízos à nossa comunidade e do entorno.',
  paragraph5: 'Também queremos respeito para com os moradores e as moradoras. Queremos que a prefeitura converse conosco quando um novo equipamento público for instalado na nossa região e que ouça os pedidos que estamos fazendo há tempos!!',
  paragraph6: 'Apoie essa causa, assine nosso manifesto e envie um e-mail para os vereadores e prefeito cobrando deles uma posição contrária ao pátio.',
  paragraph7: 'O MSRT é um grupo autônomo suprapartidário e plurirreligioso formado por lideranças comunitárias do Satélite Íris com o objetivo de discutir, planejar e realizar ações que tragam benefícios para o território de forma articulada e integrada com a Rede Local.',
}

export const email = {
  from: 'mobilizaredescobrir@gmail.com',
  subject: 'Não queremos pátio de veículos apreendidos no Satélite !',
  message: 'Prezado sr. Prefeito, srs. Vereadores e sras. Vereadoras,\n\nNós, do coletivo Mobiliza Satélite – Redescobrir para Transformar (MSRT), gostaríamos de solicitar apoio a nossa causa de NÃO instalação do pátio de veículos apreendidos no nosso bairro Satélite Íris. Esse e-mail é gerado por cada pessoa que nos apoia na causa. Caso queira ser removido/a desta lista, nos responda com a sua posição favorável ou contrária a implantação do pátio da Emdec em nosso bairro. \nO que precisamos aqui não é um pátio de veículos e sim mais lazer, mais cultura, mais esporte. Obrigado/a. \n\nColetivo Mobiliza Satélite – Redescobrir para Transformar (MSRT) O MSRT é um grupo autônomo suprapartidário e plurirreligioso formado por lideranças comunitárias do Satélite Íris com o objetivo de discutir, planejar e realizar ações que tragam benefícios para o território de forma articulada e integrada com a Rede Local.\n\nmobilizaredescobrir@gmail.com\n\nVisite a página da mobilização em https://naoqueropatio.web.app/'
}

export const contactEmail = 'mobilizaredescobrir@gmail.com';
