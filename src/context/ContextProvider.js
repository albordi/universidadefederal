import React, { useState, useEffect } from 'react';
import { Aplication } from "../enviroment";
import { recipientsProd, email} from '../constants/Data';
import { recipientsTest} from '../constants/DataTest';

// import { recipients, email } from Aplication === 'Production' ? '../constants/Data' : '../constants/DataTest';

import { useNavigate } from 'react-router-dom';
import { collection, getDocs, doc, setDoc, query, where } from "firebase/firestore";
import { db } from './firebase';
export const Context = React.createContext();

const recipients = Aplication === 'Development' ? recipientsTest : recipientsProd;

const ContextProvider = (props) => {
  const [citizens, setCitizens] = useState([]);
  const [signaturesNumber, setSignaturesNumber] = useState(0);

  useEffect(() => {
    async function fetchData() {
      // const citizensAux = [...citizens];
      const citizensAux = [];
      const querySnapshot = await getDocs(collection(db, "citizens"));
      setSignaturesNumber(querySnapshot.size);

      querySnapshot.forEach((doc) => {
        const newCitizen = {
          id: doc.id,
          name: doc.data().name,
          rg: doc.data().rg,
        };
        citizensAux.push(newCitizen);
      });
      setCitizens(citizensAux);
    }
    fetchData();
  }, []);

  const navigate = useNavigate();

  const sendEmail = (citizenName) => {
    // console.log('Inicio do envio do e-mail');
    const message = email.message + '\n\n ' + citizenName;
    recipients.map((recipient) => {
      // const url = `http://localhost:5001/send-email?from=${email.from}&to=${recipient.address}&subject=${email.subject}&message=${message}`;
      // console.log(url);
      if (recipient.position != 'Contra') {
        const url = `https://bordisendmail.herokuapp.com/send-email?from=${email.from}&to=${recipient.address}&subject=${email.subject}&message=${message}`;
        fetch(url)
          .then((res) => {
            // console.log('E-mail enviado com sucesso');
          })
          .catch((error) => {
            // console.log('Erro');
            // console.log(error);
          });
      }
      return;
    });

  }

  const storeCitizen = async (event) => {
    event.preventDefault();
    const citizenName = event.target.fullName.value;
    const q = query(collection(db, "citizens"), where("rg", "==", event.target.rg.value));
    const querySnapshot = await getDocs(q);
    if (!querySnapshot.empty) {
      alert("Esse documento já foi assinado com esse RG.")
      return;
    }
    const id = citizens.length;
    const newCitizen = {
      id: id,
      nome: event.target.fullName.value,
      rg: event.target.rg.value,
      phone: event.target.phone.value
    };
    const citizensAux = [...citizens];
    citizensAux.push(newCitizen);
    setCitizens(citizensAux);
    // console.log('Gravação do cidadão');
    await setDoc(doc(db, "citizens", id.toString()), {
      id: id,
      name: event.target.fullName.value,
      rg: event.target.rg.value.toLowerCase(),
      phone: event.target.phone.value
    })
      .then((res) => console.log('Registro gravado com sucesso: ', res))
      .catch((error) => console.log('Erro ao gravar registro', error));

    // sendEmail(citizenName);
    navigate('/finalpage');
  }

  return (
    <Context.Provider value={{
      citizens: citizens,
      signaturesNumber: signaturesNumber,
      storeCitizen: storeCitizen,
    }}>
      {props.children}
    </Context.Provider>
  );
};

export default ContextProvider;