import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { Aplication } from "../enviroment";

const firebaseConfigProd = {
  apiKey: "AIzaSyBX5pQL3BZi5Q6msdTrny3eOjwl37xhO30",
  authDomain: "federalemcampinas.firebaseapp.com",
  projectId: "federalemcampinas",
  storageBucket: "federalemcampinas.appspot.com",
  messagingSenderId: "139606394551",
  appId: "1:139606394551:web:12a99c0dfb61ab7e66ef32"
};

const firebaseConfigDev = {
  apiKey: "AIzaSyAsuKdB-FFi9GCgRn8UsdgonYQv7DaCUz0",
  authDomain: "armazenamento-6d0ae.firebaseapp.com",
  projectId: "armazenamento-6d0ae",
  storageBucket: "armazenamento-6d0ae.appspot.com",
  messagingSenderId: "213384271920",
  appId: "1:213384271920:web:adea5cbab64e0ff665b5f5"  
}

// Initialize Firebase

const firebaseConfig = Aplication === 'Development' ? firebaseConfigDev : firebaseConfigProd;

export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);