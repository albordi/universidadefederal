import {
  BrowserRouter, Routes, Route,
} from 'react-router-dom';
import Layout from './components/layout/Layout';
import FinalPage from './components/finalpage/FinalPage';
import ContextProvider from './context/ContextProvider';

import './App.css';


function App() {
  return (
    <div className="App">
      <BrowserRouter>

        <ContextProvider>
          <Routes>
            <Route path="/" element={<Layout />} />
            <Route path="/finalpage" element={<FinalPage />} />
            <Route
              path="*"
              element={
                <main style={{ padding: "1rem" }}>
                  <p>Não há nada aqui.</p>
                </main>
              }
            />
          </Routes>
        </ContextProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
