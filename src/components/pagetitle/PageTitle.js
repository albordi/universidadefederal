import { mobTitle } from '../../constants/Data';
import './PageTitle.css';
const PageTitle = () => {
  return (
    <section className='PageTitle' >
      <h1>{mobTitle}</h1>
    </section>
  )
};

export default PageTitle;