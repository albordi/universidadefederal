import { useNavigate } from 'react-router-dom';
import './FinalPage.css';
// import Logo from '../../assets/images/logocecilio.png';
// import { contactEmail } from '../../constants/Data';
import { ReactComponent as WhatsappIcon } from '../../assets/images/whatsapp.svg';
import { ReactComponent as FacebookIcon } from '../../assets/images/facebook.svg';
import { ReactComponent as TwitterIcon } from '../../assets/images/twitter.svg';

const FinalPage = () => {
  const navigate = useNavigate();

  const redirect = () => {
    navigate('/');
  };

  return (
    <section className='FinalPage'>
      <p className='title'>Obrigado por apoiar a nossa causa!</p>
      <p className='text'>Campinas precisa de uma universidade federal.</p>
      <p className='sub-title'>Compartilhe a nossa mobilização!</p>
      <a
        href='https://www.facebook.com/sharer/sharer.php?u=https://federalemcampinas.web.app/'  
        // href='https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fnaoqueropatio.web.app%2F&amp;src=sdkpreparse'
        rel='noreferrer'
        target='_blank'
      >
        <div className='Icon-Box'>
          <FacebookIcon className='FacebookIcon' />
        </div>
      </a>
      <a
        href='whatsapp://send?text=https://federalemcampinas.web.app/'
        target='_blank'
        rel='noreferrer'
      >
        <div className='Icon-Box'>
          <WhatsappIcon className='WhatsappIcon' />
        </div>
      </a>
      <a
        // href='https://twitter.com/intent/tweet?url=https://federalemcampinas.web.app/'

        href='https://twitter.com/intent/tweet?url=https://federalemcampinas.web.app/&text=Eu apoio uma Universidade Federal em Campinas'

        target='_blank'
        rel='noreferrer'
      >
        <div className='Icon-Box'>
          <TwitterIcon className='TwitterIcon' />
        </div>
      </a>
      {/* <div className='Image-Container'>
        <img src={Logo} alt='Logo do Mobiliza Satélite' /><br />
      </div> */}
      <div className='contacts'>
        <p>
          <strong>Contatos da Frente Parlamentar</strong>
        </p>
        <p>
          Vereador <strong>Cecílio Santos</strong> (coordenador da frente parlamentar): 
          <a href='mailto:cecilio.santos@campinas.sp.leg.br'>
            cecilio.santos@campinas.sp.leg.br
          </a>
        </p>
        <br />
        <p>
          Vereadora Guida Calixto:
          <a href='mailto:guida.calixto@campinas.sp.leg.br'>
            guida.calixto@campinas.sp.leg.br
          </a>
        </p>
        <p>
          Vereador Gustavo Petta:
          <a href='mailto:gustavopetta@campinas.sp.leg.br'>
            gustavopetta@campinas.sp.leg.br
          </a>
        </p>
        <p>
          Vereadora Mariana Conti:
          <a href='mailto:marianaconti@campinas.sp.leg.br'>
            marianaconti@campinas.sp.leg.br
          </a>
        </p>
        <p>
          Vereadora Paolla Miguel:{' '}
          <a href='mailto:paolla.miguel@campinas.sp.leg.br'>
            paolla.miguel@campinas.sp.leg.br
          </a>
        </p>
        <p>
          Vereador Paulo Bufalo:{' '}
          <a href='mailto:paulobufalo@campinas.sp.leg.br'>
            paulobufalo@campinas.sp.leg.br
          </a>
        </p>
        <br /><br />
        {/* <p>
          Professor Doutor Marcos Francisco Martins:
          <a href='mailto:'>
          </a>
        </p> */}
      </div>
      {/* <p>Contato: {contactEmail}</p> */}
      <button className='Button' onClick={() => redirect()}>
        Voltar para a página da mobilização
      </button>
    </section>
  );
};

export default FinalPage;
