import { mobSubTitle, mobText } from '../../constants/Data';
import Pdf from '../../assets/Sobre a necessidade da Universidade Federal de Campinas - 13.09.2023PDF.pdf'
import './MobText.css';

const MobText = () => {
  return (
    <section className='MobText' >
      <h2 className='text-center text-bold emphasis'>{mobSubTitle}</h2>
      <p>{mobText.paragraph1}</p>
      <p>{mobText.paragraph2}</p>
      <p>{mobText.paragraph3}</p>
      <p>{mobText.paragraph4}</p>
      <p>{mobText.paragraph5}</p>
      <p>{mobText.paragraph6}</p>
      {/* <small><a href='https://correio.rac.com.br/campinasermc/patio-de-veiculos-deve-ser-transferido-para-satelite-iris-1.1287700' target = "_blank" rel="noreferrer">Matéria do Correio Popular</a></small><br /> */}
      <small><a href={Pdf} target = "_blank" rel="noreferrer">Documento base para o pedido</a></small>

    </section>
  );
};

export default MobText;

// import React, { Component } from 'react';
// import Pdf from '../Documents/Document.pdf';

// class Download extends Component {

//   render() {

//     return (
//         <div className = "App">
//           <a href = {Pdf} target = "_blank">Download Pdf</a>
//         </div>
//     );

//   }
// }