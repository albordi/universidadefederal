import { Aplication } from "../../enviroment";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFaceSmile, faFaceSadCry } from '@fortawesome/free-solid-svg-icons';
import { recipientsProd} from '../../constants/Data';
import { recipientsTest} from '../../constants/DataTest';

import './Councilors.css';
const recipients = Aplication === 'Development' ? recipientsTest : recipientsProd;
const Councilors = () => {

  return (
    <section className='Councilors' >
      <p className='sub-title'>Posição das autoridades - Vereadores, vereadoras e prefeito</p>
      <table className='Councilors-Table'>
        <thead className='Table-Head'>
          <tr>
            <th>
              Apoiam nossa causa
              <FontAwesomeIcon
                className='FaceSmile Icon'
                icon={faFaceSmile}
                size="2x"
              />

            </th>
            <th>Ainda não se manifestaram
              <FontAwesomeIcon
                className='FaceNotAnswered Icon'
                icon={faFaceSadCry}
                size="2x"
              />
            </th>
            <th>Contrários à nossa causa
              <FontAwesomeIcon
                className='FaceSadCry Icon'
                icon={faFaceSadCry}
                size="2x"
              />
            </th>
          </tr></thead>
        <tbody>
          <tr><td>
            {
              recipients.map((recipient, i) => {
                // console.log(recipient);
                return (<p key={i}>{recipient.position === 'Contra' ? recipient.name : null}</p>)
              })
            }
          </td>
            <td>
              {
                recipients.map((recipient, i) => {
                  // console.log(recipient);
                  return (<p key={i}>{recipient.position === 'Sem posição' ? recipient.name : null}</p>)
                })
              }
            </td>
            <td>
              {
                recipients.map((recipient, i) => {
                  // console.log(recipient);
                  return (<p key={i}>{recipient.position === 'A favor' ? recipient.name : null}</p>)
                })
              }
            </td>

          </tr>
        </tbody>
      </table>
    </section>
  );
};

export default Councilors;