import photo from '../../assets/images/fotoprincipal.png';
import './Photo.css';

const Photo = () => {
  return (
    <section className='Photo' >
        <img src={photo} alt='Foto do pátio'/>
        <p>Plenária dos movimentos sociais pela criação da universidade federal em Campinas</p>
    </section>
  )
};

export default Photo;