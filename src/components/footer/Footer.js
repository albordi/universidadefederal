// import MobilizaSateliteImage from '../../assets/images/mobilizasatelite.png';
// import Logo from '../../assets/images/logocecilio.png';
// import { contactEmail } from '../../constants/Data';
import './Footer.css';

const Footer = () => {
  return (
    <section className='Footer'>
      <h2>Apoiam a criação da uma universidade federal em Campinas</h2>
      <ul>
        <li>
          Frente Parlamentar pela Criação da Universidade Federal de Campinas
        </li>
        <li>
          <strong>
            vereador Cecílio Santos (Coordenador da frente parlamentar
          </strong>{' '}
          - PT - Partido dos Trabalhadores)
        </li>
        <li>
          vereadora Paolla Miguel (PT) - vereadora Guida Calixto (PT) -
          vereadora Mariana Conte (PSOL - Partido Socialismo e Liberdade) -
          vereador Paulo Bufalo (PSOL) - vereador Gustavo Petta (PCdoB -
          Partido Comunista do Brasil)
        </li>

        <li>
          Prof. Dr. Marcos Francisco Martins (Professor Associado da UFSCar -
          Universidade Federal de São Carlos)
        </li>

        <li>
          cidadãos(ãs) articulados(as) aos cursinhos populares e que atuam em
          regiões periféricas da cidade de Campinas: Irineu: Cursinho Amelinha
          Tolles; Silas: Cursinho Hebert de Souza; Leopoldo: Cursinho Coletivo
          Vida Nova.
        </li>
      </ul>
      {/* <img src={Logo} alt='Logo do Mobiliza Satélite' />
      <a href={contactEmail}><p>Contato: {contactEmail}</p></a> */}
    </section>
  );
};

export default Footer;
