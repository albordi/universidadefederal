import VideoFaixa from '../../assets/video/videofaixa.mp4';
import poster from '../../assets/images/postervideo.png';

const Video = () => {
    return (
        // <video className="Video" controls poster={photo}>
        <video className="Video" width="90%" controls poster={poster} >
            <source src={VideoFaixa} type="video/mp4" />
            Your browser does not support the video tag.
        </video>
    )
}

export default Video;