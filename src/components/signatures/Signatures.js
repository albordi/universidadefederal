import { useContext } from 'react';
import { Context } from '../../context/ContextProvider';

import './Signatures.css';
const Signatures = () => {

  const { citizens } = useContext(Context);

  return (
    <section className='Signatures' >
      <p className='sub-title'>Pessoas que já apoiaram</p>
      <table className='Signatures-Table'>
        <thead className='Table-Head'><tr><th>Nome</th><th>RG</th></tr></thead>
        <tbody>
          {
            citizens.map((citizen, i) => {
              return (
                <tr key={i}><td>{citizen.name}</td><td className='text-right'>{citizen.rg}</td></tr>
              )
            })
          }
        </tbody>
      </table>
    </section>
  );
};

export default Signatures;