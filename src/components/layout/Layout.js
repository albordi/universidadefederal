import PageTitle from '../pagetitle/PageTitle';
import Photo from '../photo/Photo';
import MobText from '../mobtext/MobText';
import CallToAction from '../calltoaction/CallToAction';
import Footer from '../footer/Footer';

import './Layout.css';
import Signatures from '../signatures/Signatures';
// import Councilors from '../councilors/Councilors';

const Layout = () => {
  return (
    <section className='Layout' >
      <PageTitle />
      {/* <Photo /> */}
      <div className='Video-Container'>
        {/* <Video /> */}
        <Photo />
      </div>
      <div className='Mob-Container'>
        <MobText />
        <CallToAction />
      </div>
      {/* <Councilors /> */}
      <Signatures />
      <Footer />
    </section> 
  );
};

export default Layout;