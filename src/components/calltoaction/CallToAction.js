import { useState, useContext } from 'react';
import InputMask from 'react-input-mask';
import { Context } from '../../context/ContextProvider';
import { email } from '../../constants/Data';
import './CallToAction.css';

// function RGInput(props) {
//   return (
//     <InputMask
//       mask='(+1) 999 999 9999'
//       value={props.value}
//       onChange={props.onChange}>
//     </InputMask>
//   );
// }

function validateRg(rg) {
  rg = rg.replace('-', '').replace('.', '').replace('.', '');
  var digitos = rg.split('');
  const digVerificador = digitos.pop();
  var totais = [];
  var total = 0;
  // Multiplicamos os que seriam da primeira linha com os da segunda
  digitos.forEach(function (digito, index) {
    totais.push(Number(digito) * (2 + index));
  });
  // Multiplicamos as colunas
  totais.forEach(function (numero) {
    total += numero;
  });
  // Descobrimos o resto da divisão
  var resto = total % 11;
  if (11 - resto == digVerificador) return true;
  if ((11 - resto == 11) & (digVerificador == 0)) return true;
  if ((11 - resto == 10) & (digVerificador.toLowerCase() == 'x')) return true;
  return false;
}

function validatePhoneNumberBrazil(phoneNumber) {
  // Remove all non-digit characters
  const digitsOnly = phoneNumber.replace(/\D/g, '');

  // Check if the phone number has 11 or 10 digits (with or without the leading 0)
  if (digitsOnly.length === 11 || digitsOnly.length === 10) {
    // Check if it matches the expected pattern
    const regex = /^(?:\(\d{2}\)\s?)?\d{4,5}-\d{4}$/;
    return regex.test(phoneNumber);
  }

  return false; // Invalid length
}

const CallToAction = () => {
  const [form, setForm] = useState({
    fullName: { value: '', error: true, errorMessage: '' },
    rg: { value: '', error: true, errorMessage: '' },
    phone: { value: '', error: true, errorMessage: '' },
    emailMessage: { value: email.message, error: false, errorMessage: '' },
  });

  const [formErrorMessage, setFormErrorMessage] = useState('');

  const [formIsValid, setFormIsValid] = useState(false);

  const { storeCitizen, signaturesNumber } = useContext(Context);

  const validateForm = () => {
    setFormIsValid(false);
    if (!form.fullName.error && !form.rg.error && !form.emailMessage.error) {
      setFormIsValid(true);
    }
  };

  const handleChangeName = (e) => {
    setFormErrorMessage('');
    const newFormAux = { ...form };
    newFormAux.fullName.value = e.target.value;
    if (e.target.value === '') {
      newFormAux.fullName.error = true;
      newFormAux.fullName.errorMessage = 'Nome é obrigatório.';
    } else {
      newFormAux.fullName.error = false;
      newFormAux.fullName.errorMessage = '';
    }
    validateForm();
    setForm(newFormAux);
  };

  const handleChangeRG = (e) => {
    setFormErrorMessage('');
    const newFormAux = { ...form };
    newFormAux.rg.value = e.target.value;
    const isRgValid = validateRg(e.target.value);
    if (!isRgValid) {
      newFormAux.rg.error = true;
      newFormAux.rg.errorMessage = 'Digite um rg válido.';
    } else {
      newFormAux.rg.error = false;
      newFormAux.rg.errorMessage = '';
    }
    validateForm();
    setForm(newFormAux);
  };

  const handleChangePhone = (e) => {
    setFormErrorMessage('');
    const newFormAux = { ...form };
    newFormAux.phone.value = e.target.value;
    const isPhoneValid = validatePhoneNumberBrazil(e.target.value);
    if (!isPhoneValid) {
      newFormAux.phone.error = true;
      newFormAux.phone.errorMessage = 'Digite um telefone válido.';
    } else {
      newFormAux.phone.error = false;
      newFormAux.phone.errorMessage = '';
    }
    validateForm();
    setForm(newFormAux);
  };

  const handleChangeEmailMessage = (e) => {
    setFormErrorMessage('');
    const newFormAux = { ...form };
    newFormAux.emailMessage.value = e.target.value;
    if (e.target.value === '') {
      newFormAux.emailMessage.error = true;
      newFormAux.emailMessage.errorMessage = 'Mensagem do e-mail é obrigatória';
    } else {
      newFormAux.emailMessage.error = false;
      newFormAux.emailMessage.errorMessage = '';
    }
    validateForm();
    setForm(newFormAux);
  };

  const handleAddCitizen = (e) => {
    e.preventDefault();
    // console.log('Add citizen handler');
    if (formIsValid) {
      storeCitizen(e);
    } else {
      setFormErrorMessage('Preencha os campos corretamente antes de enviar.');
    }
  };

  return (
    <section className='CallToAction'>
      <h2 className='Head'>Apoie essa causa</h2>
      <p className='Text'>
        Ao clicar no botão abaixo você assina o abaixo assinado apoiando a
        criação de uma universidade federal em Campinas.{' '}
      </p>
      <form className='Form' onSubmit={handleAddCitizen}>
        <label htmlFor='fullName' className='hide'>
          Nome
        </label>
        <input
          className='Input'
          type='text'
          placeholder='Nome completo'
          name='fullName'
          value={form.fullName.value}
          onChange={handleChangeName}
        />
        {form.fullName.error ? (
          <p className='ErrorMessage'>{form.fullName.errorMessage}</p>
        ) : null}
        <label htmlFor='fullName' className='hide'>
          RG
        </label>
        <InputMask
          className='Input'
          mask='99.999.999-*'
          value={form.rg.value}
          onChange={handleChangeRG}
          name='rg'
          placeholder='Digite o seu rg'
        ></InputMask>
        <InputMask
          className='Input'
          mask='99 9999 99999'
          value={form.phone.value}
          onChange={handleChangePhone}
          name='phone'
          placeholder='Digite o seu telefone (Opcional)'
        ></InputMask>
        <small>O telefone será usado para atualizações da campanha</small>
        {form.rg.error ? (
          <p className='ErrorMessage'>{form.rg.errorMessage}</p>
        ) : null}
        {/* <RGInput value={form.rg} onChange={handleChangeRG}></RGInput> */}
        <label htmlFor='emailMessage' className='hide'>
          Mensagem de email
        </label>
        <textarea
          className='TextArea'
          name='emailMessage'
          value={form.emailMessage.value}
          onChange={handleChangeEmailMessage}
        >
          {form.emailMessage}
        </textarea>
        {form.emailMessage.error ? (
          <p className='ErrorMessage'>{form.emailMessage.errorMessage}</p>
        ) : null}
        <button className='Button'>Assinar</button>
        {formErrorMessage ? (
          <p className='ErrorMessage'>{formErrorMessage}</p>
        ) : null}
      </form>
      <p className='SignaturesNumber'>
        {signaturesNumber} Pessoas já assinaram
      </p>
      <small>
        Os dados coletados aqui serão usados somente para o fim desta
        mobilização.
      </small>
    </section>
  );
};

export default CallToAction;
